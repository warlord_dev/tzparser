defmodule TzparserWeb.GitController do
  use TzparserWeb, :controller

  alias Tzparser.Parser

  def index(conn, _params ) do
      h2 = Parser.get_parsed()
      render(conn, "index.html", h2: h2)
  end

  def with_min_stars(conn, %{"min_stars" => min_stars} ) do
    h2 = Parser.get_all_parsed_by_min_stars(min_stars)
    render(conn, "index.html", h2: h2)
  end

end
