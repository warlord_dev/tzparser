defmodule TzparserWeb.Router do
  use TzparserWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TzparserWeb do
    pipe_through :browser
    get "/", GitController, :index
    get "/:min_stars", GitController, :with_min_stars
  end

  # Other scopes may use custom stacks.
  # scope "/api", TzparserWeb do
  #   pipe_through :api
  # end
end
