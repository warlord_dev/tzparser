defmodule Tzparser.Repo do
  use Ecto.Repo,
    otp_app: :tzparser,
    adapter: Ecto.Adapters.MyXQL
end
