defmodule Tzparser.Parser.GitParser do
  alias Tzparser.Parser.Li
  alias Tzparser.Parser.H2
  @moduledoc """
  Documentation for Parser.
  """


  def parse() do
    Tzparser.Parser.truncate_all
    parse_h2()
  end

  def parse_h2() do
    HTTPoison.start
    url= "https://github.com/h4cc/awesome-elixir"
    case HTTPoison.get(url, [], follow_redirect: true ) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        {:ok, parsed} = Floki.parse_fragment(body)
        Floki.find(parsed, ".Box-body > .markdown-body ul:first-of-type > li > ul > li > a ")
        |> Enum.map(fn {_, href, name } ->
          h2 = %H2{name: Enum.at(name, 0), href: elem(Enum.at(href,0),1)}
          Tzparser.Parser.create_h2_from_struct(h2)
          parse_li(h2.name)
        end)
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts "++NOT FOUND++"
      {:ok, %HTTPoison.Response{status_code: 403}} ->
        IO.puts "++UNAUTHORIZED ACCESS++"
      {:ok, %HTTPoison.Response{status_code: 502}} ->
        IO.puts "++BAD GATEWAY++"
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
    end
  end

  def parse_li(tag) do
    HTTPoison.start
    url= "https://github.com/h4cc/awesome-elixir"
    case HTTPoison.get(url, [], follow_redirect: true ) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        {:ok, parsed} = Floki.parse_document(body)
        tag = String.to_charlist(tag)
        z = Enum.join(["(", "'", tag, "'", ")"], "")
        Floki.find(parsed,  ".Box-body > .markdown-body > h2:fl-contains#{z} + p + ul > li > a")
        |> Enum.map(fn {_, href, name } ->
        li = %Li{name: Enum.at(name, 0), href: elem(Enum.at(href,0),1), tag: List.to_string(tag)}
        parse_href(li)
        end)
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts "++NOT FOUND++"
      {:ok, %HTTPoison.Response{status_code: 403}} ->
        IO.puts "++UNAUTHORIZED ACCESS++"
      {:ok, %HTTPoison.Response{status_code: 502}} ->
        IO.puts "++BAD GATEWAY++"
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
    end
    end

  def parse_href(li) do
    if validate_href(li.href) do
      case HTTPoison.get(li.href, [], follow_redirect: true ) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        {:ok, parsed} = Floki.parse_fragment(body)
        Tzparser.Parser.create_li_from_struct(%{li | stars: find_stars(parsed), last_commit: find_lats_commit(parsed)})
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts "++NOT FOUND++"
      {:ok, %HTTPoison.Response{status_code: 403}} ->
        IO.puts "++UNAUTHORIZED ACCESS++"
      {:ok, %HTTPoison.Response{status_code: 502}} ->
        IO.puts "++BAD GATEWAY++"
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
        end
      else
        IO.inspect(:error)
    end
  end

  def validate_href(href) do
    cond do
      String.codepoints(href) |> Enum.at(0) == "#" -> :false
      href_have_dot_in_end(href) == "." -> :false
      true -> :true
      end
  end

  def href_have_dot_in_end(href) do
      ln = String.length(href)
      String.codepoints(href) |> Enum.at(ln - 4)
  end

  def find_stars(parsed) do
    a =  Floki.find(parsed, "a.js-social-count")
    text = Floki.text(a)
    if text == "" do
    0
    else
    String.replace(text," ", "")
    |> String.replace("\n", "")
    |> String.replace(".", "")
    |> String.replace("k", "00")
    |> Integer.parse
    |> elem(0)
    end
  end

  def find_lats_commit(parsed) do
    Floki.attribute(parsed, "relative-time.no-wrap", "datetime")
    |> Enum.at(0)
  end

  def body_fixture() do
    href = "https://github.com/dalmatinerdb/dflow"
    case HTTPoison.get(href, [], follow_redirect: true ) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        {:ok, parsed} = Floki.parse_fragment(body)
        body
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts "++NOT FOUND++"
      {:ok, %HTTPoison.Response{status_code: 403}} ->
        IO.puts "++UNAUTHORIZED ACCESS++"
      {:ok, %HTTPoison.Response{status_code: 502}} ->
        IO.puts "++BAD GATEWAY++"
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
    end
  end

end

