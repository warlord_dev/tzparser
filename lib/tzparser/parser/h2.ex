defmodule Tzparser.Parser.H2 do
  use Ecto.Schema
  import Ecto.Changeset

  schema "h2" do
    field :href, :string
    field :name, :string
    timestamps()
  end

  @doc false
  def changeset(header, attrs) do
    header
    |> cast(attrs, [:name, :href])
    |> validate_required([:name, :href])
  end
end
