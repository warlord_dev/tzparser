defmodule Tzparser.Parser.Li do
  use Ecto.Schema
  import Ecto.Changeset

  schema "li" do
    field :href, :string
    field :last_commit, :string
    field :name, :string
    field :stars, :integer
    field :tag , :string
    timestamps()
  end

  @doc false
  def changeset(parsed, attrs) do
    parsed
    |> cast(attrs, [:name, :href, :stars, :last_commit])
    |> validate_required([:name, :href, :stars])
  end
end
