defmodule Tzparser.Parser.ParseTask do
    use GenServer

    def start_link(_) do
      GenServer.start_link(__MODULE__, %{})
    end

    @impl true
    def init(state) do
      Tzparser.Parser.GitParser.parse
      schedule_work()
      {:ok, state}
    end

    @impl true
    def handle_info(:work, state) do
      Tzparser.Parser.GitParser.parse
      schedule_work()
      {:noreply, state}
    end

    defp schedule_work do
      # In 2 hours
      Process.send_after(self(), :work, 24 * 60 * 60 * 1000)
    end
end
