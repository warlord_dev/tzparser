defmodule Tzparser.Parser do

  import Ecto.Query, warn: false
  alias Tzparser.Repo
  alias Tzparser.Parser.Li
  alias Tzparser.Parser.H2

  def truncate_all() do
    Repo.query("TRUNCATE h2", [])
    Repo.query("TRUNCATE li", [])
  end

  def list_li do
    Repo.all(Li)
  end

  def get_parsed() do
    h2 = list_h2()
    for x <- h2, do: {x, get_all_li_by_h2_name(x.name)}
  end

  def get_all_parsed_by_min_stars(stars) do
    h2 = list_h2()
    for x <- h2, do: {x, get_all_li_by_stars(x.name, stars)}
  end

  def get_all_li_by_stars(name, stars) do
    query = from li in Li , where: li.tag == ^name and li.stars >= ^stars
    Repo.all(query)
  end

  def create_li_from_struct(%Li{} = li) do
    Li.changeset(li, %{})
    |> Repo.insert()
  end

  def create_h2_from_struct(%H2{} = h2) do
    H2.changeset(h2, %{})
    |> Repo.insert
  end

  def get_all_li_by_h2_name(name) do
    query = from li in Li , where: li.tag == ^name
    Repo.all(query)
  end

  def list_h2 do
    Repo.all(H2)
  end

end
