defmodule Tzparser.UlTest do
  use Tzparser.DataCase


  describe "li" do
    alias  Tzparser.Parser
    alias  Tzparser.Parser.Li

    @valid_attrs %Li{href: "www.git.com", last_commit: "2012-12-12", name: "test", stars: 12, tag: "Actors"}
    @invalid_attrs %Li{href: "123", last_commit: "12", name: "nil", stars: 2, tag: "Watch"}

    def li_fixture(attrs \\ %{}) do
      {:ok, li} = Parser.create_li_from_struct(@valid_attrs)
      li
    end

    test "create_li valid" do
      assert {:ok, %Li{} = li} = Parser.create_li_from_struct(@valid_attrs)
      assert li.href == "www.git.com"
      assert li.name == "test"
      assert li.last_commit == "2012-12-12"
      assert li.stars == 12
      assert li.tag == "Actors"
    end

    test "list_li valid" do
      li = li_fixture()
      assert Parser.list_li() == [li]
    end

  end

end
