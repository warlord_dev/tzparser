defmodule Tzparser.H2Test do
  use Tzparser.DataCase

  describe "h2" do
    alias  Tzparser.Parser
    alias Tzparser.Parser.H2

    @valid_attrs %H2{name: "test", href: "#Test"}
    @invalid_attrs %H2{name: "32112", href: "#Nest"}

    def h2_fixture(attrs \\ %{}) do
            {:ok, h2} =
            Parser.create_h2_from_struct(@valid_attrs)
            h2
    end

    test "create_h2" do
      assert {:ok, %H2{} = h2} = Parser.create_h2_from_struct(@valid_attrs)
      assert h2.href == "#Test"
      assert h2.name == "test"
    end

    test "list_h2" do
      h2 = h2_fixture()
      assert Parser.list_h2() == [h2]
    end

  end

end
