defmodule Tzparser.GitParserTest do
  use Tzparser.DataCase
  alias Tzparser

  alias Tzparser.Parser
  alias Tzparser.Parser.Li

  describe "li" do

    def body_fixture() do
      href = "https://github.com/dalmatinerdb/dflow"
      case HTTPoison.get(href, [], follow_redirect: true ) do
        {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
          {:ok, parsed} = Floki.parse_fragment(body)
          body
        {:ok, %HTTPoison.Response{status_code: 404}} ->
          IO.puts "++NOT FOUND++"
        {:ok, %HTTPoison.Response{status_code: 403}} ->
          IO.puts "++UNAUTHORIZED ACCESS++"
        {:ok, %HTTPoison.Response{status_code: 502}} ->
          IO.puts "++BAD GATEWAY++"
        {:error, %HTTPoison.Error{reason: reason}} ->
          IO.inspect reason
      end
    end

    test "find all li valid" do
      li = Tzparser.Parser.GitParser.parse_li("Actors")
      assert li = Tzparser.Parser.GitParser.parse_li("Actors")
    end

    test "parse li href" do
      li = Tzparser.Parser.GitParser.parse_href(%Li{name: "dflow", href:	"https://github.com/dalmatinerdb/dflow"})
      assert li = Tzparser.Parser.GitParser.parse_href(%Li{name: "dflow", href:	"https://github.com/dalmatinerdb/dflow"})
    end

    test "find li stars" do
     body = body_fixture()
     li = Tzparser.Parser.GitParser.find_stars(body)
     assert li = Tzparser.Parser.GitParser.find_stars(body)
    end


  end

end
