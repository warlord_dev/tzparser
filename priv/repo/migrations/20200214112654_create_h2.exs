defmodule Tzparser.Repo.Migrations.CreateH2 do
  use Ecto.Migration

  def change do
    create table(:h2) do
      add :name, :string
      add :href, :string
      timestamps()
    end

  end
end
