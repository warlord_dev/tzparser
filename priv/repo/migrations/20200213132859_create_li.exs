defmodule Tzparser.Repo.Migrations.CreateLi do
  use Ecto.Migration

  def change do
    create table(:li) do
      add :name, :string
      add :href, :string
      add :stars, :integer
      add :last_commit, :string
      add :tag, :string
      timestamps()
    end

  end
end
